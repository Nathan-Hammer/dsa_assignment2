import ballerinax/kafka;
import ballerina/io;

kafka:ConsumerConfiguration consumerConfiguration3 = {
    groupId: "group-id",
    offsetReset: "earliest",
    // Subscribes to the topic `test-kafka-topic`.
    topics: ["test-kafka-topic"]
};

public type Order3 record {|
    int orderId;
    string productName;
    decimal price;
    boolean isValid;
|};

// Create a subtype of `kafka:AnydataConsumerRecord`.
public type OrderConsumerRecord2 record {|
    *kafka:AnydataConsumerRecord;
    Order value;
|};

kafka:Consumer orderConsumer3 = check new (kafka:DEFAULT_URL, consumerConfiguration);

public function main2() returns error? {
    // Polls the consumer for order records.
    OrderConsumerRecord[] records = check orderConsumer->poll(1);

    check from OrderConsumerRecord orderRecord in records
        where orderRecord.value.isValid
        do {
            io:println(orderRecord.value.productName);
        };
}
